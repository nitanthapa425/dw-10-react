import { useState } from "react";
import "./App.css";
import Age from "./practiceComponent/Age";
import ButtonClick from "./practiceComponent/ButtonClick";
import Details from "./practiceComponent/Details";
import Details1 from "./practiceComponent/Details1";
import EffectOnDifferentData from "./practiceComponent/EffectOnDifferentData";
import Info from "./practiceComponent/Info";
import Increment from "./practiceComponent/LeaarnUseStateHook/Increment";
import LearnUseState2 from "./practiceComponent/LeaarnUseStateHook/LearnUseState2";
import { LearnUseStateHook1 } from "./practiceComponent/LeaarnUseStateHook/LearnUseStateHook1";
import ShowAndHide from "./practiceComponent/LeaarnUseStateHook/ShowAndHide";
import Toggle from "./practiceComponent/LeaarnUseStateHook/Toggle";
import WhyUseState from "./practiceComponent/LeaarnUseStateHook/WhyUseState";
import LearnCleanUpFunction from "./practiceComponent/LeanrUseEffecetHook/LearnCleanUpFunction";
import LearnUseEffectHook from "./practiceComponent/LeanrUseEffecetHook/LearnUseEffectHook";
import LearnMap1 from "./practiceComponent/LearnMap1";
import LearnMap2 from "./practiceComponent/LearnMap2";
import LearnTearnary from "./practiceComponent/LearnTearnary";
import Name from "./practiceComponent/Name";
import MyLinks from "./practiceComponent/MyLinks";
import MyRoutes from "./practiceComponent/MyRoutes";
import Form1 from "./practiceComponent/Form/Form1";
import Form2 from "./practiceComponent/Form/Form2";
import ReactLink from "./practiceComponent/ReactLink";
import ReactRouter from "./practiceComponent/ReactRouter";
import Form3 from "./practiceComponent/Form/Form3";
import Learn1UseRefHook from "./practiceComponent/LearnUseRefHook/Learn1UseRefHook";
import AddToLocalStorage from "./practiceComponent/learnLocalStorage/AddToLocalStorage";
import GetLocalStorageData from "./practiceComponent/learnLocalStorage/GetLocalStorageData";
import RemoveLocalStorageData from "./practiceComponent/learnLocalStorage/RemoveLocalStorageData";
import AddDataToSessionStorage from "./practiceComponent/learnSessionStorage/AddDataToSessionStorage";
import GetDataOfSessionStorage from "./practiceComponent/learnSessionStorage/GetDataOfSessionStorage";
import RemoveDataFromSessionStorage from "./practiceComponent/learnSessionStorage/RemoveDataFromSessionStorage";

function App() {
  let a = <p>i am paragraph</p>;
  let name = "nitan";
  let age = 29;
  let b = `${1 + 1}`; //`2`

  let [show, setShow] = useState(true);
  return (
    <div>
      {/* <p style={{ color: "green", backgroundColor: "red" }}>
        This is paragraph
      </p>
      <h1>heading 1</h1>
      <span>nitan</span>
      <span>thapa</span>

      <div className="success">Hello i am div</div>

      <a href="https://www.facebook.com/" target=" ">
        facebook
      </a>

      <div>
        hello my nam
        <br></br>
        is nitan thapa
      </div>

      <div style={{ backgroundColor: "green" }}>Hello</div> */}

      {/* <img src="./logo192.png"></img> */}
      {/* <Name></Name> */}
      {/* <Age></Age> */}
      {/* <Details name="nitan" address="gagalphedi" age={29}></Details> */}

      {/* <Details1 name="nitan" address="gagalphedi" age={29}>
        {" "}
      </Details1> */}

      {/* <LearnTearnary></LearnTearnary> */}

      {/* <EffectOnDifferentData></EffectOnDifferentData> */}
      {/* <Info name="nitan" age={29} fatherDetail={{fname:"shiva", fage:60}} favFood={["mutton","chicken"]}></Info> */}
      {/* <LearnMap1></LearnMap1> */}
      {/* <LearnMap2></LearnMap2> */}

      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseStateHook1></LearnUseStateHook1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <ShowAndHide></ShowAndHide> */}
      {/* <Toggle></Toggle> */}
      {/* <WhyUseState></WhyUseState> */}
      {/* <Increment></Increment> */}
      {/* <LearnUseEffectHook></LearnUseEffectHook> */}

      {/* {show ? <LearnCleanUpFunction></LearnCleanUpFunction> : null}

      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button>

      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button> */}

      {/* <MyLinks></MyLinks> */}

      {/* <MyRoutes></MyRoutes> */}

      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}

      {/* <ReactRouter></ReactRouter> */}

      {/* <Form3></Form3> */}
      {/* <Learn1UseRefHook></Learn1UseRefHook> */}
      {/* <AddToLocalStorage></AddToLocalStorage> */}
      {/* <GetLocalStorageData></GetLocalStorageData> */}
      {/* <RemoveLocalStorageData></RemoveLocalStorageData> */}

      {/* <AddDataToSessionStorage></AddDataToSessionStorage> */}
      {/* <GetDataOfSessionStorage></GetDataOfSessionStorage> */}
      <RemoveDataFromSessionStorage></RemoveDataFromSessionStorage>
    </div>
  );
}

export default App;

/* 
css
  inline
    obj is used for styling purposes
    you must use camel case convention
  external

*/
