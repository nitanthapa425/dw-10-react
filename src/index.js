import React, { createContext } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./learncss.css";
import { BrowserRouter } from "react-router-dom";
import MyApp from "./MyApp";
// import React, { createContext } from "react";

// Create a context with a default value (optional)

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <div>
    <BrowserRouter>
      <React.StrictMode>
        <MyApp></MyApp>
      </React.StrictMode>
    </BrowserRouter>
  </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

/* 
My profile
  link => /admin/my-profile
  route => /admin/my-profile => AdminProfile
  AdminProfile
    hit api on page load  (useEffect)
    token => get token from local storage

Logout
Link  /admin/logout
Route  /admin/logout  AdminLogout
AdminLogout
  remove token form localStorage
  redirect to /





Update profile
  make update profile button on my profile when clicked change link to admin/profile-update 
  route admin/profile-update   UpdateProfile
  UpdateProfile
    make a form  email*,password*,role*
    button => Update  (hit api )
      
    for data populate  => get myProfile api on page load and set data


Update password
    link    =>  admin/update-password
    route  => admin/update-password => AdminUpdatePassword
    AdminUpdatePassword
      make a form  for oldPassword, newPassword
      make a update button
        hit api
        logout  (remove token form localstorage)
        login page


forgot password
      forgot password (button)  click  ("/admin/forgot-password")
      component  ("/admin/forgot-password")   AdminForgotPassword
      AdminForgotPassword
          email

reset password
Route  /reset-password    AdminResetPassword
AdminResetPassword
  password
redirect to /admin/login

login (if login) (show)
myProfile
update-password
logout
update-profile(*****************)


logout
show login
show register




read all user =admin => superadmin
read specific user =>  admin =>superadin
delete user superadin
update user  => admin => superadmin











*/
