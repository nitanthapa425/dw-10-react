import React from "react";

const Details1 = ({ name, address, age }) => {
  return (
    <div style={{ backgroundColor: "green" }}>
      name is {name}
      <br></br>
      address is {address}
      <br></br>
      age is {age}
    </div>
  );
};

export default Details1;
