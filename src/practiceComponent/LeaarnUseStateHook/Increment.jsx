import React, { useState } from "react";

const Increment = () => {
  let [count, setCount] = useState(0); //0//1//1

  let [count2, setCount2] = useState(100); //100//100//101

  let handleIncrement = (e) => {
    setCount(count + 1);
  };

  let handeleIncrement2 = (e) => {
    setCount2(count2 + 1);
  };

  return (
    <div>
      count is {count}
      <br></br>
      connt2 is {count2}
      <br></br>
      <button onClick={handleIncrement}>Increment</button>
      <button onClick={handeleIncrement2}>Increment count 2</button>
    </div>
  );
};

export default Increment;
