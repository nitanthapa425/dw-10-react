import React, { useState } from "react";

const LearnUseState2 = () => {
  let [count, setCount] = useState(0);

  let increment = (e) => {
    if (count < 10) {
      setCount(count + 1);
    }
  };
  let decrement = (e) => {
    if (count > 0) {
      setCount(count - 1);
    }
  };
  let reset = (e) => {
    setCount(0);
  };
  return (
    <div>
      {count}
      <br></br>
      <button onClick={increment}>Increment</button>
      <br></br>
      <button onClick={decrement}>Decrement</button>
      <br></br>
      <button onClick={reset}>Reset</button>
    </div>
  );
};

export default LearnUseState2;
