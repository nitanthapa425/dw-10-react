import React, { useState } from "react";

const WhyUseState = () => {
  let [name, setName] = useState("nitan");
  //nitan//hari
  console.log("************");

  return (
    <div>
      {name}
      <br></br>

      <button
        onClick={() => {
          setName("hari");
        }}
      >
        Change Name
      </button>
    </div>
  );
};

export default WhyUseState;
