import React, { useState } from "react";

const Toggle = () => {
  let [showImg, setShowImg] = useState(true);

  let handleImage = (e) => {
    if (showImg === true) {
      setShowImg(false);
    } else {
      setShowImg(true);
    }
  };
  return (
    <div>
      {showImg ? <img src="./logo192.png"></img> : null}

      <button onClick={handleImage}>
        {showImg === true ? "Hide" : "Show"}
      </button>
    </div>
  );
};

export default Toggle;

// show (true) =>  HIde
// hide (false) =>Show
