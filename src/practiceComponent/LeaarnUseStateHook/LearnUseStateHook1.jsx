import React, { useState } from "react";

//define variable using useState
// call variable
// change variable
export const LearnUseStateHook1 = () => {
  // let name= "nitan"

  let [name, setName] = useState("nitan");
  let [age, setAge] = useState(26);

  let handleClick = (e) => {
    // name = "ram";
    setName("ram");
  };
  let handleAge = (e) => {
    // name = "ram";
    setAge(27);
  };

  return (
    <div>
      my name is {name}
      <br></br>
      <button onClick={handleClick}>Change Name</button>
      <br />i am {age}
      <br />
      <button onClick={handleAge}>change Age</button>
    </div>
  );
};
