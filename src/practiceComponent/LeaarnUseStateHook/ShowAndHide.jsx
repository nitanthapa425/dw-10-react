import React, { useState } from "react";

const ShowAndHide = () => {
  let [showImg, setShowImg] = useState(true);

  //   let showImgFun = () => {
  //     setShowImg(true);
  //   };
  //   let hideImgFun = () => {
  //     setShowImg(false);
  //   };

  let handleImg = (isVisible) => {
    return (e) => {
      setShowImg(isVisible);
    };
  };

  return (
    <div>
      {showImg ? <img src="./logo192.png" alt="logo"></img> : null}
      <br></br>
      <button onClick={handleImg(true)}>ShowImage</button>
      <br />
      <button onClick={handleImg(false)}>HideImage</button>
    </div>
  );
};

// handleImg        (e)=>{}            if you dont need to pass value
// handleImg()      ()=>{ return ((e)=>{})}   if you need to pass value

export default ShowAndHide;
