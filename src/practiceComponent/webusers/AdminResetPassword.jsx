import axios from "axios";
import React, { useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminResetPassword = () => {
  let [password, setPassword] = useState("");

  let navigate = useNavigate();
  let [params] = useSearchParams();
  let token = params.get("token");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      password: password,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/reset-password`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="password"> Password: </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>

        <button style={{ cursor: "pointer" }} type="submit">
          Reset
        </button>
      </form>
    </div>
  );
};

export default AdminResetPassword;
