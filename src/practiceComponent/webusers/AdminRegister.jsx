import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";

const AdminRegister = () => {
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("male");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      email: email,
      password: password,
      dob: dob,
      gender: gender,
    };

    // console.log(data);

    data = {
      ...data,
      role: "admin",
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users`,
        method: "POST",
        data: data,
      });
      toast.success(
        "A Link has been sent to you email . Please click the given link to verify your account."
      );
      setFullName("");
      setEmail("");
      setPassword("");
      setDob("");
      setGender("male");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">FullName: </label>
          <input
            type="text"
            placeholder="Eg: nitan"
            id="name"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email : </label>
          <input
            type="email"
            placeholder="Eg: nitanthapa425@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            placeholder="Eg: abc@1234"
            id="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="dob">Dob: </label>
          <input
            type="date"
            id="dob"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label>Gender</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default AdminRegister;
