import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const UpdateSpecificUser = () => {
  let [fullName, setFullName] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("male");
  let navigate = useNavigate();
  let token = localStorage.getItem("token");

  let params = useParams();
  let id = params.id;

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      dob: dob,
      gender: gender,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/${id}`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      navigate(`/admin/${id}`);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  let getAdminUser = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/${id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      //   setUser(result.data.data);
      let data = result.data.data;
      setDob(data.dob);
      setFullName(data.fullName);
      setGender(data.gender);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminUser();
  }, []);
  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">FullName: </label>
          <input
            type="text"
            placeholder="Eg: nitan"
            id="name"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="dob">Dob: </label>
          <input
            type="date"
            id="dob"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label>Gender</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

        <button type="submit">Update</button>
      </form>
    </div>
  );
};

export default UpdateSpecificUser;
