import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminLogin = () => {
  let navigate = useNavigate();

  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      email: email,
      password: password,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/login`,
        method: "POST",
        data: data,
      });

      let token = result.data.token;
      localStorage.setItem("token", token);

      navigate("/admin");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="email">Email : </label>
          <input
            type="email"
            placeholder="Eg: nitanthapa425@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            placeholder="Eg: abc@1234"
            id="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>

        <button style={{ cursor: "pointer" }} type="submit">
          Login
        </button>
        <div
          style={{ cursor: "pointer" }}
          onClick={() => {
            navigate("/admin/forgot-password");
          }}
        >
          Forgot Password
        </div>
      </form>
    </div>
  );
};

export default AdminLogin;

// login
/* 
make form
hit api (token)
save token to local storage
/admin
 */

// profile
