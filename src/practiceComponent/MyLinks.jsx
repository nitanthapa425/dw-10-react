import React from "react";
import { NavLink } from "react-router-dom";

const MyLinks = () => {
  return (
    <div>
      <NavLink to="/products/create" style={{ marginRight: "20px" }}>
        Create Product
      </NavLink>
      <NavLink to="/products" style={{ marginRight: "20px" }}>
        Product
      </NavLink>
      <NavLink to="/students/create" style={{ marginRight: "20px" }}>
        Create Student
      </NavLink>
      <NavLink to="/students" style={{ marginRight: "20px" }}>
        Student
      </NavLink>
    </div>
  );
};

/* 
create Product  => "http://localhost:3000/products/create"
Product  => "http://localhost:3000/products"

create Student => "http://localhost:3000/students/create"

Student => "http://localhost:3000/students"

*/

export default MyLinks;
