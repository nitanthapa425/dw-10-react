import React, { useRef } from "react";

const Learn1UseRefHook = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let refInput1 = useRef();
  return (
    <div>
      <div
        onClick={() => {
          refInput1.current.focus();
        }}
      >
        Focus Input1
      </div>
      <div
        onClick={() => {
          refInput1.current.blur();
        }}
      >
        Blur Input1
      </div>

      <button
        onClick={(e) => {
          ref1.current.style.backgroundColor = "red";
          ref1.current.style.color = "white";
        }}
      >
        Change Bg of Babu
      </button>

      <button
        onClick={(e) => {
          ref2.current.style.backgroundColor = "blue";
        }}
      >
        Change Bg of Nani
      </button>

      <div ref={ref1}>Babu</div>
      <div ref={ref2}>Nani</div>

      <input ref={refInput1}></input>
      <input></input>
    </div>
  );
};

export default Learn1UseRefHook;
