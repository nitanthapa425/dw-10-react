import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadAllProduct = () => {
  let [products, setProduct] = useState([]);
  let navigate = useNavigate();

  let getAllProducts = async () => {
    let result = await axios({
      url: "http://localhost:8000/products",
      method: "GET",
    });

    setProduct(result.data.result);
  };

  useEffect(() => {
    getAllProducts();
  }, []);

  return (
    <div>
      {products.map((item, i) => {
        console.log(item);
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p> product is {item.name}</p>
            <p> product price NRs. {item.price}</p>
            <p> product quantity {item.quantity}</p>
            <button
              style={{ marginRight: "30px" }}
              onClick={() => {
                navigate(`/products/${item._id}`);
              }}
            >
              View
            </button>
            <button
              style={{ marginRight: "30px" }}
              onClick={(e) => {
                navigate(`/products/update/${item._id}`);
              }}
            >
              Edit
            </button>
            <button
              style={{ marginRight: "30px" }}
              onClick={async () => {
                let result = await axios({
                  url: `http://localhost:8000/products/${item._id}`,
                  method: "DELETE",
                });

                getAllProducts();
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllProduct;

/* 

get all products
url: "http://localhost:8000/products"
method:"GET"


get specific products
url: "http://localhost:8000/products/1234214"
method:"GET"

*/
