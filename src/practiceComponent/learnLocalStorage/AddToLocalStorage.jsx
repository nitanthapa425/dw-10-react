import React from "react";

const AddToLocalStorage = () => {
  let token = "12341234123";

  localStorage.setItem("token", token);
  localStorage.setItem("name", "nitan");
  localStorage.setItem("age", "29");
  localStorage.setItem("isMarried", "false");

  return <div>AddToLocalStorage</div>;
};

export default AddToLocalStorage;
