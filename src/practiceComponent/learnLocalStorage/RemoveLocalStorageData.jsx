import React from "react";

const RemoveLocalStorageData = () => {
  return (
    <div>
      <button
        onClick={() => {
          localStorage.removeItem("token");
        }}
      >
        Remove Token
      </button>
      <button
        onClick={() => {
          localStorage.removeItem("name");
        }}
      >
        Remove Name
      </button>
      <button
        onClick={() => {
          localStorage.removeItem("isMarried");
        }}
      >
        Remove IsMarried
      </button>
      <button
        onClick={() => {
          localStorage.removeItem("age");
        }}
      >
        Remove Age
      </button>
    </div>
  );
};

export default RemoveLocalStorageData;

//Local storage is the browser's memory for a particular url
//Tha data in a Local storage persist even when the session end (browser close )(tab close)
