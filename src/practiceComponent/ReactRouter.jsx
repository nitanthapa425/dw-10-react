import React, { useContext, useState } from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import ReadAllProduct from "./product/ReadAllProduct";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import CreateProduct from "./product/CreateProduct";
import ReadAllStudent from "./student/ReadAllStudent";
import CreateStudent from "./student/CreateStudent";
import ReadSpecificStudent from "./student/ReadSpecificStudent";
import UpdateStudent from "./student/UpdateStudent";
import UpdateProduct from "./product/UpdateProduct";
import ReactLink from "./ReactLink";
import AdminRegister from "./webusers/AdminRegister";
import AdminVerify from "./webusers/AdminVerify";
import AdminLogin from "./webusers/AdminLogin";
import AdminProfile from "./webusers/AdminProfile";
import AdminLogout from "./webusers/AdminLogout";
import AdminProfileUpdate from "./webusers/AdminProfileUpdate";
import AdminUpdatePassword from "./webusers/AdminUpdatePassword";
import AdminForgotPassword from "./webusers/AdminForgotPassword";
import AdminResetPassword from "./webusers/AdminResetPassword";
import ReadAllUser from "./webusers/ReadAllUser";
import ReadSpecificUser from "./webusers/ReadSpecificUser";
import UpdateSpecificUser from "./webusers/UpdateSpecificUser";

// / => Home page
// /products => read all products
// /products/:id => detail page
// /products/create => create products
// /products/update/:Id => products update

// /students => read all products
// /students/:id => detail page
// /students/create => create products
// /students/update/:id => products update

const ReactRouter = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <ReactLink></ReactLink>
              <Outlet></Outlet>
              {/* <div>This is Footer</div> */}
            </div>
          }
        >
          <Route
            path="reset-password"
            element={<AdminResetPassword></AdminResetPassword>}
          ></Route>
          <Route index element={<div>Home Page</div>}></Route>

          <Route
            path="verify-email"
            element={<AdminVerify></AdminVerify>}
          ></Route>

          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>
            <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route>

          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudent></ReadAllStudent>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>
            <Route
              path="create"
              element={<CreateStudent></CreateStudent>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>

          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route
              path="update-password"
              element={<AdminUpdatePassword></AdminUpdatePassword>}
            ></Route>
            <Route
              path="profile-update"
              element={<AdminProfileUpdate></AdminProfileUpdate>}
            ></Route>
            <Route path="logout" element={<AdminLogout></AdminLogout>}></Route>
            <Route
              path="my-profile"
              element={<AdminProfile></AdminProfile>}
            ></Route>
            <Route index element={<div>This is admin dash board</div>}></Route>
            <Route
              path="register"
              element={<AdminRegister></AdminRegister>}
            ></Route>
            <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
            <Route
              path="forgot-password"
              element={<AdminForgotPassword></AdminForgotPassword>}
            ></Route>

            <Route
              path="read-all-user"
              element={<ReadAllUser></ReadAllUser>}
            ></Route>
            <Route
              path=":id"
              element={<ReadSpecificUser></ReadSpecificUser>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateSpecificUser></UpdateSpecificUser>}
              ></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default ReactRouter;
