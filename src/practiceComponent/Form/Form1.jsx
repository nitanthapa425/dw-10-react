import React, { useState } from "react";

const Form1 = () => {
  let [name, setName] = useState("");

  let onSubmit = (e) => {
    e.preventDefault();

    let data = {
      name: name,
    };

    console.log(data);
  };
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            type="text"
            placeholder="Eg: nitan"
            id="name"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>

        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default Form1;

//name => text
//surname => text
//email => email
//password => password
//phoneNumber => number
//dob => date
//description  <textarea><textarea>
