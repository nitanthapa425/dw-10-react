import React from "react";

const Details = (props) => {
  console.log(props);
  return (
    <div>
      name is {props.name}
      <br></br>
      address is {props.address}
      <br></br>
      age is {props.age}
    </div>
  );
};

export default Details;
